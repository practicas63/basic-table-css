# basic-table-CSS
Development practice of a basic table with CSS, structure, thead, tbody, tfoot and caption for the name of the table.

![table start](img/table-start.png)

where sticky positioning is applied so as not to lose the name of the table and the names of the columns when scrolling to the table if it contains too much information.

![table sticky](img/table-sticky.png)

With hover styles to know the row in which we are positioned.

![table hover](img/table-hover.png)